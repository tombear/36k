const fs = require("fs-extra");

const importHTML = path => fs.readFileSync(path, "utf8");

const importHTMLs = dir =>
  fs.readdirSync(dir)
    .reduce((acc, v) => [ ...acc, importHTML(`${dir}/${v}`) ], []);

const [ before, after ] = importHTMLs("./shell");
const shellWrap = x => before + x + after;

fs.ensureDirSync("./public");

fs.readdirSync("./pages")
  .reduce((acc, v) => {
    return [
      ...acc,
      {
        name: v,
        data: importHTML(`./pages/${v}`)
      }
    ]
  }, [])
  .forEach(({ name, data }) => {
    fs.writeFileSync(`./public/${name}`, shellWrap(data));
  });

fs.copySync("./assets/", "./public/assets/")
